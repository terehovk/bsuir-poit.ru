<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'no-reply@example.com',
    'emailConfirmTokenExpire' => 259200, // 3 days
    'passwordResetTokenExpire' => 3600,
];
