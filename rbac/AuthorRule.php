<?php

namespace app\rbac;

use app\modules\user\models\User;
use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * User group rule class.
 */
class AuthorRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'isAuthor';


    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return isset($params['model']) ? $params['model']['user_id'] == $user : false;
    }
}
