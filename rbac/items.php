<?php
return [
    'showNews' => [
        'type' => 2,
        'description' => 'Show news',
    ],
    'showAllNews' => [
        'type' => 2,
        'description' => 'Show all news',
        'children' => [
            'showNews',
        ],
    ],
    'showGroupNews' => [
        'type' => 2,
        'description' => 'Show group news',
        'ruleName' => 'isAuthorGroup',
        'children' => [
            'showNews',
        ],
    ],
    'createNews' => [
        'type' => 2,
        'description' => 'Create news',
    ],
    'createGroupNews' => [
        'type' => 2,
        'description' => 'Create group news',
        'children' => [
            'createNews',
        ],
    ],
    'createAllNews' => [
        'type' => 2,
        'description' => 'Create all news',
        'children' => [
            'createNews',
        ],
    ],
    'updateNews' => [
        'type' => 2,
        'description' => 'Update news',
    ],
    'updateOwnNews' => [
        'type' => 2,
        'description' => 'Update own news',
        'ruleName' => 'isAuthor',
        'children' => [
            'updateNews',
        ],
    ],
    'updateGroupNews' => [
        'type' => 2,
        'description' => 'Update group news',
        'ruleName' => 'isAuthorGroup',
        'children' => [
            'updateNews',
        ],
    ],
    'deleteNews' => [
        'type' => 2,
        'description' => 'Delete news',
    ],
    'deleteOwnNews' => [
        'type' => 2,
        'description' => 'Delete own news',
        'ruleName' => 'isAuthor',
        'children' => [
            'deleteNews',
        ],
    ],
    'deleteGroupNews' => [
        'type' => 2,
        'description' => 'Delete group news',
        'ruleName' => 'isAuthorGroup',
        'children' => [
            'deleteNews',
        ],
    ],
    'guest' => [
        'type' => 1,
        'description' => 'Guest',
        'ruleName' => 'userGroup',
    ],
    'unconfirmed_user' => [
        'type' => 1,
        'description' => 'Unconfirmed User',
        'ruleName' => 'userGroup',
    ],
    'user' => [
        'type' => 1,
        'description' => 'User',
        'ruleName' => 'userGroup',
        'children' => [
            'showGroupNews',
            'unconfirmed_user',
        ],
    ],
    'trusted' => [
        'type' => 1,
        'description' => 'Trusted',
        'ruleName' => 'userGroup',
        'children' => [
            'createGroupNews',
            'updateOwnNews',
            'deleteOwnNews',
            'user',
        ],
    ],
    'captain' => [
        'type' => 1,
        'description' => 'Captain',
        'ruleName' => 'userGroup',
        'children' => [
            'deleteGroupNews',
            'updateGroupNews',
            'trusted',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Admin',
        'ruleName' => 'userGroup',
        'children' => [
            'showAllNews',
            'createAllNews',
            'updateNews',
            'deleteNews',
            'captain',
        ],
    ],
];
