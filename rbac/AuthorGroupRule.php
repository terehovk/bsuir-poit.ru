<?php

namespace app\rbac;

use app\models\User;
use Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * User group rule class.
 */
class AuthorGroupRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'isAuthorGroup';


    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            /**
             * @var User $identity
             */
            $identity = Yii::$app->user->identity;
            $groupId = $identity->student_group_id;
            return isset($params['model']) ? $params['model']['student_group_id'] == $groupId : false;
        }
        return false;
    }
}
