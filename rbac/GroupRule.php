<?php

namespace app\rbac;

use app\models\User;
use \Yii;
use yii\rbac\Item;
use yii\rbac\Rule;

/**
 * User group rule class.
 */
class GroupRule extends Rule
{
    /**
     * @inheritdoc
     */
    public $name = 'userGroup';

    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            /**
             * @var User $identity
             */
            $identity = Yii::$app->user->identity;
            $role = $identity->role;

            if ($item->name === 'admin') {
                return $role === User::ROLE_ADMIN;
            } elseif ($item->name === 'captain') {
                return $role === User::ROLE_CAPTAIN || $role === User::ROLE_ADMIN;
            } elseif ($item->name === 'trusted') {
                return $role === User::ROLE_TRUSTED || $role === User::ROLE_CAPTAIN || $role === User::ROLE_ADMIN;
            } elseif ($item->name === 'user') {
                return $role === User::ROLE_USER || $role === User::ROLE_TRUSTED || $role === User::ROLE_CAPTAIN || $role === User::ROLE_ADMIN;
            } elseif ($item->name === 'unconfirmed_user') {
                return $role === User::ROLE_UNCONFIRMED_USER || $role === User::ROLE_USER || $role === User::ROLE_TRUSTED || $role === User::ROLE_CAPTAIN || $role === User::ROLE_ADMIN;
            } elseif ($item->name === 'guest') {
                return $role === User::ROLE_UNCONFIRMED_USER || $role === User::ROLE_USER || $role === User::ROLE_TRUSTED || $role === User::ROLE_CAPTAIN || $role === User::ROLE_ADMIN;
            }
        } else
            return $item->name === 'guest';
        return false;
    }
}
