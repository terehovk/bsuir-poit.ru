<?php

namespace app\controllers;

use app\models\form\EnrollForm;
use Yii;
use app\models\Event;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'enroll', 'event',],
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                    [
                        'actions' => ['admin', 'create', 'update',],
                        'allow' => true,
                        'roles' => ['trusted'],
                    ],
                    [
                        'actions' => ['delete',],
                        'allow' => true,
                        'roles' => ['captain'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $data = Event::findAllByGroup();

        return $this->render('index', [
            'data' => $data,
        ]);
    }

    public function actionEnroll()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new EnrollForm();
        if ($model->load(Yii::$app->request->post()) && $model->enroll()) {
            return [
                'result' => 'ok',
            ];
        }
        return [
            'result' => 'error',
            'error' => $model->getFirstError('eventId'),
        ];
    }

    public function actionEvent($id)
    {
        $event = Event::find()->where([
            'id' => $id,
        ])->one();
        if (!$event) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->renderPartial('_template', [
            'event' => $event,
        ]);
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Event::find()->where(
                (!Yii::$app->user->can('admin') ? ['student_group_id' => Yii::$app->user->identity->student_group_id] : [])
            ),
        ]);

        return $this->render('admin', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Event();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
