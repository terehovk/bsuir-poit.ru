<?php

namespace app\controllers;

use app\models\Account;
use app\models\form\LoginForm;
use app\models\form\PasswordResetForm;
use app\models\form\PasswordResetRequestForm;
use app\models\form\SignupForm;
use app\models\form\EmailConfirmForm;
use app\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\authclient\clients\VKontakte;
use yii\base\InvalidParamException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class UserController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['signup', 'error', 'auth', 'login', 'email-confirm', 'password-reset-request', 'password-reset', 'success-callback'],
                        'allow' => true,
                        'roles' => ['guest'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin', 'change-role'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function actionAdmin()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('admin', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function successCallback(ClientInterface $client)
    {
        $attributes = $client->getUserAttributes();
        $client_id = null;
        if ($client instanceof VKontakte) {
            $client_id = $attributes['user_id'];
        }
        $account = Account::find()->where(['client' => $client->getId(), 'client_id' => $client_id])->one();
        if ($account) {
            /**
             * @var Account $account
             */
            if ($account->user_id != 0) {
                $user = User::findIdentity($account->user_id);
                if ($user)
                    Yii::$app->user->login($user, 3600 * 24 * 30);
                return $this->goBack();
            } else {
                return $this->redirect(['/user/signup', 'connection' => $account->connection_string]);
            }


        } else {
            $account = new Account();
            $account->client = $client->id;
            $account->client_id = $attributes['user_id'];
            $account->client_data = serialize($attributes);
            $account->user_id = 0;
            $account->connection_string = Yii::$app->security->generateRandomString();
            $account->status = Account::STATUS_WAIT;
            if ($account->save()) {
                return $this->redirect(['/user/signup', 'connection' => $account->connection_string]);
            }
        }
        return $this->redirect(['/user/login']);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm(Yii::$app->request->get('connection', null));
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_EMAIL_CONFIRM_REQUEST'));
                return $this->goHome();
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionEmailConfirm($token)
    {
        try {
            $model = new EmailConfirmForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->confirmEmail()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_EMAIL_CONFIRM_SUCCESS'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'FLASH_EMAIL_CONFIRM_ERROR'));
        }
        return $this->goHome();
    }

    public function actionPasswordResetRequest()
    {
        $model = new PasswordResetRequestForm(\Yii::$app->params['passwordResetTokenExpire']);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_PASSWORD_RESET_REQUEST'));
                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'FLASH_PASSWORD_RESET_ERROR'));
            }
        }
        return $this->render('passwordResetRequest', [
            'model' => $model,
        ]);
    }

    public function actionPasswordReset($token)
    {
        try {
            $model = new PasswordResetForm($token, \Yii::$app->params['passwordResetTokenExpire']);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'FLASH_PASSWORD_RESET_SUCCESS'));
            return $this->goHome();
        }
        return $this->render('passwordReset', [
            'model' => $model,
        ]);
    }

    public function actionChangeRole($id, $role)
    {
        $user = User::find()->where([
            'id' => $id,
        ])->one();
        if (!$user)
            return $this->redirect('/admin/user/');
        /**
         * @var User $user
         */
        if (in_array($role, [
            User::ROLE_UNCONFIRMED_USER,
            User::ROLE_USER,
            User::ROLE_TRUSTED,
            User::ROLE_CAPTAIN,
        ])) {
            $user->role = $role;
            $user->save();
        }

        return $this->redirect('/user/admin');
    }
}
