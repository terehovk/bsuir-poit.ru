<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Created by PhpStorm.
 * User: mbv
 * Date: 6.3.16
 * Time: 20.28
 */


class BootstrapMaterialAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-material-design/dist';
    public $css = [
        'css/ripples.min.css',
        YII_ENV_DEV ? 'css/material.css' : 'css/material.min.css',
    ];
    public $js = [
        YII_ENV_DEV ? 'js/ripples.js' : 'js/ripples.min.js',
        YII_ENV_DEV ? 'js/material.js' : 'js/material.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];



}