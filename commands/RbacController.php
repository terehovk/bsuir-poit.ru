<?php
namespace app\commands;

use app\rbac\AuthorGroupRule;
use app\rbac\AuthorRule;
use Yii;
use yii\console\Controller;
use app\rbac\GroupRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        // Rules
        $groupRule = new GroupRule();
        $auth->add($groupRule);
        $authorRule = new AuthorRule();
        $auth->add($authorRule);
        $authorGroupRule = new AuthorGroupRule();
        $auth->add($authorGroupRule);
        // Roles

        $showNews = $auth->createPermission('showNews');
        $showNews->description = 'Show news';
        $auth->add($showNews);

        $showAllNews = $auth->createPermission('showAllNews');
        $showAllNews->description = 'Show all news';
        $auth->add($showAllNews);

        $auth->addChild($showAllNews, $showNews);

        $showGroupNews = $auth->createPermission('showGroupNews');
        $showGroupNews->description = 'Show group news';
        $showGroupNews->ruleName = $authorGroupRule->name;
        $auth->add($showGroupNews);

        $auth->addChild($showGroupNews, $showNews);

        $createNews = $auth->createPermission('createNews');
        $createNews->description = 'Create news';
        $auth->add($createNews);

        $createGroupNews = $auth->createPermission('createGroupNews');
        $createGroupNews->description = 'Create group news';
        $auth->add($createGroupNews);

        $auth->addChild($createGroupNews, $createNews);

        $createAllNews = $auth->createPermission('createAllNews');
        $createAllNews->description = 'Create all news';
        $auth->add($createAllNews);

        $auth->addChild($createAllNews, $createNews);

        $updateNews = $auth->createPermission('updateNews');
        $updateNews->description = 'Update news';
        $auth->add($updateNews);

        $updateOwnNews = $auth->createPermission('updateOwnNews');
        $updateOwnNews->description = 'Update own news';
        $updateOwnNews->ruleName = $authorRule->name;
        $auth->add($updateOwnNews);

        $auth->addChild($updateOwnNews, $updateNews);

        $updateGroupNews = $auth->createPermission('updateGroupNews');
        $updateGroupNews->description = 'Update group news';
        $updateGroupNews->ruleName = $authorGroupRule->name;
        $auth->add($updateGroupNews);

        $auth->addChild($updateGroupNews, $updateNews);

        $deleteNews = $auth->createPermission('deleteNews');
        $deleteNews->description = 'Delete news';
        $auth->add($deleteNews);

        $deleteOwnNews = $auth->createPermission('deleteOwnNews');
        $deleteOwnNews->description = 'Delete own news';
        $deleteOwnNews->ruleName = $authorRule->name;
        $auth->add($deleteOwnNews);

        $auth->addChild($deleteOwnNews, $deleteNews);

        $deleteGroupNews = $auth->createPermission('deleteGroupNews');
        $deleteGroupNews->description = 'Delete group news';
        $deleteGroupNews->ruleName = $authorGroupRule->name;
        $auth->add($deleteGroupNews);

        $auth->addChild($deleteGroupNews, $deleteNews);



        //Groups

        $guest = $auth->createRole('guest');
        $guest->description = 'Guest';
        $guest->ruleName = $groupRule->name;
        $auth->add($guest);

        $unconfirmed_user = $auth->createRole('unconfirmed_user');
        $unconfirmed_user->description = 'Unconfirmed User';
        $unconfirmed_user->ruleName = $groupRule->name;
        $auth->add($unconfirmed_user);


        $user = $auth->createRole('user');
        $user->description = 'User';
        $user->ruleName = $groupRule->name;
        $auth->add($user);

        $auth->addChild($user, $showGroupNews);

        $auth->addChild($user, $unconfirmed_user);


        $trusted = $auth->createRole('trusted');
        $trusted->description = 'Trusted';
        $trusted->ruleName = $groupRule->name;
        $auth->add($trusted);

        $auth->addChild($trusted, $createGroupNews);
        $auth->addChild($trusted, $updateOwnNews);
        $auth->addChild($trusted, $deleteOwnNews);

        $auth->addChild($trusted, $user);

        $captain = $auth->createRole('captain');
        $captain->description = 'Captain';
        $captain->ruleName = $groupRule->name;
        $auth->add($captain);

        $auth->addChild($captain, $deleteGroupNews);
        $auth->addChild($captain, $updateGroupNews);

        $auth->addChild($captain, $trusted);

        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $admin->ruleName = $groupRule->name;
        $auth->add($admin);

        $auth->addChild($admin, $showAllNews);
        $auth->addChild($admin, $createAllNews);
        $auth->addChild($admin, $updateNews);
        $auth->addChild($admin, $deleteNews);

        $auth->addChild($admin, $captain);


    }
}