<?php


/* @var $this yii\web\View */
/* @var $user app\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/email-confirm', 'token' => $user->email_confirm_token]);
?>

<?= Yii::t('app', 'HELLO {name}', ['name' => $user->getFullName()]); ?>

<?= Yii::t('app', 'FOLLOW_TO_CONFIRM_EMAIL') ?>

<?= $confirmLink ?>

<?= Yii::t('app', 'IGNORE_IF_DO_NOT_REGISTER') ?>