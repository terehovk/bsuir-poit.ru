/**
 * Created by mbv on 18.4.16.
 */

function updateEvent(id) {
    $.ajax({
        url: '/event/get',
        data: {
            id: id
        },
        dataType: 'html',
        success: function (data) {
            $('#event' + id).html(data);
        }
    })
}
$('.eventsList').on('submit', '.formEnroll form', function(e) {
    var form = $(this);
    jQuery.ajax({
        url: form.attr('action'),
        type: "POST",
        dataType: "json",
        data: form.serialize(),
        success: function(response) {
            if (response.result == "ok") {
                updateEvent(form.data('id'));
            } else {
                alert(response.error);
            }
        }
    });
    e.preventDefault();
}).on('click', '.buttonShowList', function(e) {
    var eventId = $(this).data('id');
    $('#eventRecords'+eventId).fadeToggle();
    e.preventDefault();
}).on('click', '.eventRecordsHide', function(e) {
    $(this).parent().parent().fadeToggle();
    e.preventDefault();
});

