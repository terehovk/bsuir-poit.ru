<?php
namespace app\components\grid;

use yii\grid\DataColumn;
use yii\helpers\Html;

class SelectableColumn extends DataColumn
{
    /**
     * @var array
     */
    public $itemsList = [];

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $value = $this->getDataCellValue($model, $key, $index);
        $_name = 'data_' . $this->attribute . '_' . $model->id;
        $html = Html::dropDownList($_name, $value, $this->itemsList, [
            'id' => $_name,
            'class' => 'selectableColum',
            'data-attribute' => $this->attribute,
            'data-id' => $model->id,
        ]);
        return $value === null ? $this->grid->emptyCell : $html;
    }
}