<?php
namespace app\components\grid;

use yii\grid\DataColumn;
use yii\helpers\Html;

class EditableColumn extends DataColumn
{
    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $value = $this->getDataCellValue($model, $key, $index);
        $html = Html::tag('input', '', [
            'type' => 'text',
            'value' => $value,
            'id' => 'data_' . $this->attribute . '_' . $model->id,
            'class' => 'editableColum',
            'data-attribute' => $this->attribute,
            'data-id' => $model->id,
        ]);
        return $value === null ? $this->grid->emptyCell : $html;
    }
}