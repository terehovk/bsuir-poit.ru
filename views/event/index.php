<?php

use app\models\Event;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $data Event[] */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="event-index">

        <section class="eventsList">
            <?php
            foreach ($data as $event):
                ?>
                <div id="event<?= $event->id; ?>" class="eventWrapper">
                    <?= $this->render('_template', ['event' => $event]); ?>
                </div>
                <?php
            endforeach;
            ?>
        </section>

    </div>
<?php
$this->registerJsFile('/js/main.js', ['depends' => yii\web\JqueryAsset::className()]);