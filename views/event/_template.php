<?php
use app\models\Event;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var Event $event
 */

?>
<?php
switch ($event->getStatus()):
    case Event::STATUS_WAIT:
        ?>
        <div class="event event-wait">
            <?= $event->text; ?><br/>
            Дата сдачи: <strong><?= $event->start_time; ?></strong>
            <br/>
            Начало записи: <strong><?= $event->allow_time; ?></strong>
        </div>
        <?php
        break;
    case Event::STATUS_CAN_ENROLL:
        ?>
        <div class="event event-canEnroll">
            <?= $event->text; ?><br/>
            Дата сдачи: <strong><?= $event->start_time; ?></strong>
            <hr/>
            Уже записались <strong><?= $event->getSizeQueue(); ?></strong>
            <br/><br/>
            <div class="formEnroll">
                <?php
                /**
                 * @var ActiveForm $form
                 */
                $form = ActiveForm::begin([
                    'action' => ['event/enroll'],
                    'options' => [
                        'data-id' => $event->id,
                    ],
                    'fieldConfig' => [
                        'template' => "{input}",
                    ],
                ]);
                ?>
                <?= $form->field($event->getEnrollForm(), 'eventId')->hiddenInput(); ?>
                <?= Html::submitButton('Записаться', ['class' => 'btn btn-success btn-raised buttonEventEnroll']); ?>
                <?php
                ActiveForm::end();
                ?>
            </div>
            <br/>
            <?php
            if ($event->getSizeQueue() > 0):
                ?>
                <a href="#" class="buttonShowList" data-id="<?= $event->id; ?>">посмотреть список</a>
                <?php
            endif;
            ?>
        </div>
        <?php
        break;
    case Event::STATUS_ENROLLED:
        ?>
        <div class="event event-enrolled">
            <?= $event->text; ?><br/>
            Дата сдачи: <strong><?= $event->start_time; ?></strong>
            <hr/>
            <strong>Ты уже записался:)</strong>
            <br/>
            В очереди ты - <strong><?= $event->getNumberInQueue(); ?></strong>, всего -
            <strong><?= $event->getSizeQueue(); ?></strong>
            <br/>
            <a href="#" class="buttonShowList" data-id="<?= $event->id; ?>">посмотреть список</a>
        </div>
        <?php
        break;
endswitch; ?>

<?php
if (in_array($event->getStatus(), [Event::STATUS_CAN_ENROLL, Event::STATUS_ENROLLED]) && ($event->getSizeQueue() > 0)):
    ?>
    <div class="eventRecords" id="eventRecords<?= $event->id; ?>">
        <div class="event event-records">
            <button type="button" class="close eventRecordsHide" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <?php
            $counter = 0;
            foreach ($event->getEventRecords() as $record):
                ?>
                <div class="eventRecord">
                    <strong><?= ++$counter; ?></strong>. <span
                        class="recordUser"><?= $record->user->getFullName(); ?></span> <span
                        class="recordEnrollTime">записался <?= \Yii::$app->formatter->asDatetime($record->time_enroll, 'd.MM.y HH:mm:ss'); ?></span>
                </div>
                <?php
            endforeach;
            ?>
        </div>
    </div>
    <?php
endif;
?>

