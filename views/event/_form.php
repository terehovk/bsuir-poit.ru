<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">
    <div class="well">
        <?php $form = ActiveForm::begin([
            'id' => 'event-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div class=\"col-lg-offset-1 col-lg-4\"><div class=\"label-floating\">{label}\n{input}</div></div>\n<div class=\"col-lg-7\">{error}</div>",
                'labelOptions' => ['class' => 'control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'student_group_id')->dropDownList(\app\models\StudentGroup::getList(Yii::$app->user->can('trusted') ? Yii::$app->user->identity->student_group_id : null)) ?>

        <?= $form->field($model, 'student_subgroup')->dropDownList(\app\models\User::getListStudentSubgroup(true)) ?>

        <?= $form->field($model, 'start_time', ['enableClientValidation' => false])->widget(DateTimePicker::className(), [
            'language' => 'ru',
            'convertFormat' => true,
            'removeButton' => false,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'autoclose' => true,
                'format' => "php:d.m.Y h:i",
            ]
        ]); ?>

        <?= $form->field($model, 'allow_time', ['enableClientValidation' => false])->widget(DateTimePicker::className(), [
            'language' => 'ru',
            'convertFormat' => true,
            'removeButton' => false,
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'autoclose' => true,
                'format' => "php:d.m.Y h:i",
            ]
        ]); ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'mode')->dropDownList(\app\models\Event::getListMode()) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
