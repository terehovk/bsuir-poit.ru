<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">
    <p>
        <?= (Yii::$app->user->can('updateNews', ['model' => $model])) ? Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
        <?= (Yii::$app->user->can('deleteNews', ['model' => $model])) ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) : '' ?>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading"><span
                class="news-title"><?= $model->title; ?></span><br><?= \Yii::$app->formatter->asDatetime($model->date_int, 'd MMMM y, h:mm'); ?>
        </div>
        <div class="panel-body">
            <?= $model->content; ?>
        </div>
    </div>

</div>
