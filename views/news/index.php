<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $data array */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    foreach ($data as $news):
        ?>
        <div class="panel panel-default">
            <div class="panel-heading"><span
                    class="news-title"><?= $news['title']; ?></span><br><?= \Yii::$app->formatter->asDatetime($news['date'], 'd MMMM y, h:mm'); ?>
            </div>
            <div class="panel-body">
                <?= BaseStringHelper::truncateWords($news['content'], 50); ?>
                <div><?= Html::a(Yii::t('app', 'Read more'), ['view', 'id' => $news['id']], ['class' => 'btn btn-raised btn-primary btn-sm']) ?></div>
            </div>
            <?php
            if (Yii::$app->user->can('updateNews', ['model' => $news]) || Yii::$app->user->can('deleteNews', ['model' => $news])):
                ?>
                <div class="panel-footer btn-show-news">
                    <?= (Yii::$app->user->can('updateNews', ['model' => $news])) ? Html::a(Yii::t('app', 'Update'), ['update', 'id' => $news['id']], ['class' => 'btn btn-raised btn-info btn-sm']) : '' ?>
                    <?= (Yii::$app->user->can('deleteNews', ['model' => $news])) ? Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $news['id']], [
                        'class' => 'btn btn-raised btn-danger btn-sm',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) : '' ?>
                </div>
                <?php
            endif;
            ?>
        </div>
        <?php
    endforeach;
    ?>

</div>
