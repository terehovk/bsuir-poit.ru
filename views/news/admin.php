<?php

use app\components\grid\LinkColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create News'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => LinkColumn::className(),
                'attribute' => 'title',
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'date',
                //'value' => 'textDate',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}'

            ],
        ],
    ]); ?>

</div>
