<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\form\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="well">
        <?php $form = ActiveForm::begin([
            'id' => 'form-signup',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div class=\"col-lg-offset-1 col-lg-4\"><div class=\"label-floating\">{label}\n{input}</div></div>\n<div class=\"col-lg-7\">{error}</div>",
                'labelOptions' => ['class' => 'control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'firstName')->textInput() ?>
        <?= $form->field($model, 'lastName')->textInput() ?>


        <?= $form->field($model, 'studentGroupId')->dropDownList(\app\models\StudentGroup::getList()) ?>
        <?= $form->field($model, 'studentSubgroup')->dropDownList(\app\models\User::getListStudentSubgroup()) ?>

        <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className()) ?>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton(Yii::t('app', 'SIGNUP_BUTTON'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
