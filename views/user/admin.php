<?php

use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-group-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fullName',
            'studentGroup.number',
            'student_subgroup',
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'role',
                'value' => 'textRole',
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{activation} {deactivation} {make_trusted} {make_captain} {make_user}',
                'buttons' => [
                    'activation' => function ($url, $model, $key) {
                        /**
                         * @var User $model
                         */
                        $options = [
                            'title' => 'Активировать',
                            'aria-label' => 'Активировать',
                            'data-confirm' => 'Вы действительно хотите активировать ' . $model->getFullName() . ' (' . $model->email . ')',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        $url = Url::toRoute(['/user/change-role', 'id' => $model->id, 'role' => User::ROLE_USER]);
                        $html = Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
                        return ($model->role == User::ROLE_UNCONFIRMED_USER) ? $html : '';
                    },
                    'deactivation' => function ($url, $model, $key) {
                        /**
                         * @var User $model
                         */
                        $options = [
                            'title' => 'Деактивировать',
                            'aria-label' => 'Деактивировать',
                            'data-confirm' => 'Вы действительно хотите деактивировать ' . $model->getFullName() . ' (' . $model->email . ')',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        $url = Url::toRoute(['/user/change-role', 'id' => $model->id, 'role' => User::ROLE_UNCONFIRMED_USER]);
                        $html = Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, $options);
                        return ($model->role == User::ROLE_USER) ? $html : '';
                    },
                    'make_trusted' => function ($url, $model, $key) {
                        /**
                         * @var User $model
                         */
                        $options = [
                            'title' => 'Сделать пользователя Доверенным?',
                            'aria-label' => 'Сделать пользователя Доверенным?',
                            'data-confirm' => 'Вы действительно хотите сделать пользователя ' . $model->getFullName() . ' (' . $model->email . ')  доверенным ?',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        $url = Url::toRoute(['/user/change-role', 'id' => $model->id, 'role' => User::ROLE_TRUSTED]);
                        $html = Html::a('<span class="glyphicon glyphicon-sunglasses"></span>', $url, $options);
                        return ($model->role == User::ROLE_USER || $model->role == User::ROLE_CAPTAIN) ? $html : '';
                    },
                    'make_captain' => function ($url, $model, $key) {
                        /**
                         * @var User $model
                         */
                        $options = [
                            'title' => 'Сделать пользователя Старостой?',
                            'aria-label' => 'Сделать пользователя Старостой?',
                            'data-confirm' => 'Вы действительно хотите сделать пользователя ' . $model->getFullName() . ' (' . $model->email . ')  старостой ?',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        $url = Url::toRoute(['/user/change-role', 'id' => $model->id, 'role' => User::ROLE_CAPTAIN]);
                        $html = Html::a('<span class="glyphicon glyphicon-knight"></span>', $url, $options);
                        return ($model->role == User::ROLE_USER || $model->role == User::ROLE_TRUSTED) ? $html : '';
                    },
                    'make_user' => function ($url, $model, $key) {
                        /**
                         * @var User $model
                         */
                        $options = [
                            'title' => 'Сделать пользователем?',
                            'aria-label' => 'Сделать пользователем?',
                            'data-confirm' => 'Вы действительно хотите сделать ' . $model->getFullName() . ' (' . $model->email . ') пользователем ?',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        $url = Url::toRoute(['/user/change-role', 'id' => $model->id, 'role' => User::ROLE_USER]);
                        $html = Html::a('<span class="glyphicon glyphicon-user"></span>', $url, $options);
                        return ($model->role == User::ROLE_TRUSTED || $model->role == User::ROLE_CAPTAIN) ? $html : '';
                    },
                ],
            ],
        ],
    ]); ?>

</div>
