<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\form\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="well">
        <div class="col-lg-offset-1 col-lg-11"> <?= yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['user/auth'],
                'popupMode' => false,
            ]) ?>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div class=\"col-lg-offset-1 col-lg-4\"><div class=\"label-floating\">{label}\n{input}</div></div>\n<div class=\"col-lg-7\">{error}</div>",
                'labelOptions' => ['class' => 'control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\"><div class=\"checkbox\"><label>{input} {labelTitle}</label></div></div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::a(Yii::t('app', 'LINK_PASSWORD_RESET'), ['password-reset-request']) ?>.
        </div>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
