<?php
/**
 * Created by PhpStorm.
 * User: mbv
 * Date: 7.3.16
 * Time: 9.53
 */

namespace app\widgets;


use yii\helpers\Html;

class AuthChoiceItem extends \yii\authclient\widgets\AuthChoiceItem
{
    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        $htmlOptions = [];
        $viewOptions = $this->client->getViewOptions();
        if ($this->authChoice->popupMode) {
            if (isset($viewOptions['popupWidth'])) {
                $htmlOptions['data-popup-width'] = $viewOptions['popupWidth'];
            }
            if (isset($viewOptions['popupHeight'])) {
                $htmlOptions['data-popup-height'] = $viewOptions['popupHeight'];
            }
        }
        return Html::a(Html::img('/img/vk.png'), $this->authChoice->createClientUrl($this->client), $htmlOptions);
    }

}