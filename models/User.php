<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $email
 * @property string $email_confirm_token
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $first_name
 * @property string $last_name
 * @property integer $student_group_id
 * @property integer $student_subgroup
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAIT = 2;

    const ROLE_GUEST = 0;
    const ROLE_UNCONFIRMED_USER = 1;
    const ROLE_USER = 5;
    const ROLE_TRUSTED = 10;
    const ROLE_CAPTAIN = 50;
    const ROLE_ADMIN = 100;

    const SEX_FEMALE = 1;
    const SEX_MALE = 2;
    
    const SUBGROUP_ALL = 0;
    const SUBGROUP_1 = 1;
    const SUBGROUP_2 = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'auth_key', 'password_hash', 'first_name', 'last_name',], 'required'],
            [['student_group_id', 'student_subgroup', 'role', 'status', ], 'integer'],
            [['email', 'email_confirm_token', 'password_hash', 'password_reset_token', 'first_name', 'last_name'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['email_confirm_token'], 'unique'],
            [['password_reset_token'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'USER_ID'),
            'email' => Yii::t('app', 'USER_EMAIL'),
            'first_name' => Yii::t('app', 'USER_FIRST_NAME'),
            'last_name' => Yii::t('app', 'USER_LAST_NAME'),
            'student_group_id' => Yii::t('app', 'USER_STUDENT_GROUP_ID'),
            'student_subgroup' => Yii::t('app', 'USER_STUDENT_SUBGROUP'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'fullName' => Yii::t('app', 'USER_FULL_NAME'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroup()
    {
        return $this->hasOne(StudentGroup::className(), ['id' => 'student_group_id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by Email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @param integer $timeout
     * @return static|null
     */
    public static function findByPasswordResetToken($token, $timeout)
    {
        if (!static::isPasswordResetTokenValid($token, $timeout)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @param integer $timeout
     * @return bool
     */
    public static function isPasswordResetTokenValid($token, $timeout)
    {
        if (empty($token)) {
            return false;
        }
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $timeout >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_confirm_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public function getFullName() { 
        return $this->first_name . ' ' . $this->last_name;
    }

    public static function getRoles() {
        return [
            self::ROLE_GUEST => 'Гость',
            self::ROLE_UNCONFIRMED_USER => 'Неактивированный',
            self::ROLE_USER => 'Пользователь',
            self::ROLE_TRUSTED => 'Доверенный',
            self::ROLE_CAPTAIN => 'Староста',
            self::ROLE_ADMIN => 'аДмИн',
        ];
    }

    public function getTextRole() {
        return self::getRoles()[$this->role];
    }
    
    public static function getListStudentSubgroup($withAll = false) {
        $result = [];
        if ($withAll) {
            $result[self::SUBGROUP_ALL] = 'Общее';
        }
        $result += [
            self::SUBGROUP_1 => 'Первая подгруппа',
            self::SUBGROUP_2 => 'Вторая подгруппа',
        ];
        return $result;
    }
    public static function getValuesStudentSubgroup($withAll = false) {
        $result = [];
        if ($withAll) {
            $result[] = self::SUBGROUP_ALL;
        }
        $result += [
            self::SUBGROUP_1,
            self::SUBGROUP_2,
        ];
        return $result;
    }
}
