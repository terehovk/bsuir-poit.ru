<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%event_record}}".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $user_id
 * @property string $comment
 * @property integer $time_enroll
 */
class EventRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event_record}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'user_id', 'time_enroll'], 'integer'],
            [['comment'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'comment' => Yii::t('app', 'Comment'),
            'time_enroll' => Yii::t('app', 'Time Enroll'),
        ];
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param Event $event
     * @return $this|array|EventRecord[]
     */
    public static function getRecordsForEvent($event)
    {
        $records = self::find()->joinWith('user')->where([
            'event_id' => $event->id,
        ]);
        switch ($event->mode) {
            case Event::MODE_DEFAULT:
                $records = $records->orderBy(['time_enroll' => SORT_ASC]);
                break;
            case Event::MODE_SEX_WOMEN:
                $records = $records->orderBy(['user.sex' => SORT_ASC, 'time_enroll' => SORT_ASC]);
                break;
            case Event::MODE_SEX_MEN:
                $records = $records->orderBy(['user.sex' => SORT_DESC, 'time_enroll' => SORT_ASC]);
                break;
            case Event::MODE_SUBGROUP:
                switch ($event->student_subgroup) {
                    case User::SUBGROUP_1:
                        $records = $records->orderBy(['user.student_subgroup' => SORT_ASC, 'time_enroll' => SORT_ASC]);
                        break;
                    case User::SUBGROUP_2:
                        $records = $records->orderBy(['user.student_subgroup' => SORT_DESC, 'time_enroll' => SORT_ASC]);
                        break;
                }
                break;
        }
        $records = $records->all();
        return $records;
    }
}
