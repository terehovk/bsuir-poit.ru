<?php

namespace app\models\form;


use app\models\Event;
use app\models\EventRecord;
use yii\base\Model;

class EnrollForm extends Model
{
    public $eventId;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['eventId', 'required'],
            ['eventId', 'integer'],
            ['eventId', 'validateEnroll'],
        ];
    }

    public function validateEnroll() {
        if (!$this->hasErrors()) {
            /**
             * @var Event $event
             */
            $event = Event::find()->where([
                'id' => $this->eventId,
            ])->one();
            if (!$event) {
                $this->addError('eventId', 'EVENT_NOT_FOUND');
            } elseif ($event->isUserEnroll()) {
                $this->addError('eventId', 'ALREADY_ENROLLED');
            }
        }
    }

    public function enroll() {
        if ($this->validate()) {
            $eventRecord = new EventRecord();
            $eventRecord->event_id = $this->eventId;
            $eventRecord->user_id = \Yii::$app->user->id;
            $eventRecord->time_enroll = time();
            return $eventRecord->save();
        } else {
            return false;
        }

    }

}