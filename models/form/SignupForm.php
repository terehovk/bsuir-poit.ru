<?php

namespace app\models\form;

use app\models\Account;
use app\models\StudentGroup;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;
use app\models\User;

/**
 * LoginForm is the model behind the login form.
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $firstName;
    public $lastName;
    public $studentGroupId;
    public $studentSubgroup;
    public $reCaptcha;


    private $_defaultRole = User::ROLE_UNCONFIRMED_USER;
    private $_account = null;


    /**
     * SignupForm constructor.
     * @param string $connectionString
     * @param array $config
     */
    public function __construct($connectionString, $config = [])
    {
        $this->_account = Account::findByConnectionString($connectionString);
        $this->initByAccount();
        parent::__construct($config);
    }

    private function initByAccount()
    {
        if ($this->_account) {
            switch ($this->_account->client) {
                case "vkontakte":
                    $data = unserialize($this->_account->client_data);
                    $this->firstName = $data['first_name'];
                    $this->lastName = $data['last_name'];
                    break;
            }
        }
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => Yii::t('app', 'ERROR_EMAIL_EXISTS')],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['firstName', 'required'],
            ['lastName', 'required'],

            ['studentGroupId', 'required'],
            ['studentGroupId', 'exist', 'targetClass' => StudentGroup::className(), 'targetAttribute' => 'id', 'message' => Yii::t('app', 'ERROR_STUDENT_GROUP_NOT_EXISTS')],

            ['studentSubgroup', 'required'],
            ['studentSubgroup', 'in', 'range' => User::getValuesStudentSubgroup(),],

            [['reCaptcha'], ReCaptchaValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'firstName' => Yii::t('app', 'First Name'),
            'lastName' => Yii::t('app', 'Last Name'),
            'reCaptcha' => '',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->email = $this->email;
            $user->setPassword($this->password);

            $user->first_name = $this->firstName;
            $user->last_name = $this->lastName;

            $user->student_group_id = $this->studentGroupId;
            $user->student_subgroup = $this->studentSubgroup;

            $user->status = User::STATUS_WAIT;
            $user->role = $this->_defaultRole;
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();
            if ($user->save()) {
                Yii::$app->mailer->compose(['text' => '@app/mail/emailConfirm'], ['user' => $user])
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject('Email confirmation for ' . Yii::$app->name)
                    ->send();
                if ($this->_account) {
                    $this->_account->user_id = $user->id;
                    $this->_account->status = Account::STATUS_CONNECTED;
                    $this->_account->save();
                }
            }
            return $user;
        }
        return null;
    }
}