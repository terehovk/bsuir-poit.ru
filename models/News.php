<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $photo
 * @property string $content
 * @property integer $date
 * @property integer $student_group_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class News extends ActiveRecord
{
    public $date_int;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'date', 'student_group_id'], 'required'],
            [['content'], 'string'],
            [['date', 'student_group_id', 'created_at', 'updated_at'], 'integer'],
            [['title', 'photo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->date_int = $this->date;
        $this->date = \Yii::$app->formatter->asDatetime($this->date, 'd.MM.y H:mm');

        parent::afterFind();
    }


    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->date = strtotime($this->date);
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert)
                $this->user_id = Yii::$app->user->id;
            return true;
        } else {
            return false;
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'photo' => Yii::t('app', 'Photo'),
            'content' => Yii::t('app', 'Content'),
            'date' => Yii::t('app', 'Date'),
            'student_group_id' => Yii::t('app', 'USER_STUDENT_GROUP_ID'),
            'user_id' => Yii::t('app', 'USER'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroup()
    {
        return $this->hasOne(StudentGroup::className(), ['id' => 'student_group_id']);
    }

    public static function findAllByGroup()
    {
        $request = self::find();
        if (!Yii::$app->user->can('showAllNews')) {
            $request->where(['student_group_id' => 0]);
            $request->orWhere(['student_group_id' => Yii::$app->user->identity->student_group_id]);
        }
        return $request->orderBy(['date' => SORT_DESC])->all();
    }
}
