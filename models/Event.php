<?php

namespace app\models;

use app\models\form\EnrollForm;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property integer $id
 * @property integer $student_group_id
 * @property integer $student_subgroup
 * @property integer $start_time
 * @property integer $allow_time
 * @property string $text
 * @property integer $mode
 */
class Event extends ActiveRecord
{
    const MODE_DEFAULT = 1;
    const MODE_SEX_WOMEN = 2;
    const MODE_SEX_MEN = 3;
    const MODE_SUBGROUP = 4;

    const STATUS_WAIT = 1;
    const STATUS_CAN_ENROLL = 2;
    const STATUS_ENROLLED = 3;

    private $_status = null;

    public $start_time_int;
    public $allow_time_int;

    private $_eventRecords = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_group_id', 'student_subgroup', 'start_time', 'allow_time', 'mode'], 'integer'],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student_group_id' => Yii::t('app', 'Student Group ID'),
            'student_subgroup' => Yii::t('app', 'Student Subgroup'),
            'start_time' => Yii::t('app', 'Start Time'),
            'allow_time' => Yii::t('app', 'Allow Time'),
            'text' => Yii::t('app', 'Text'),
            'mode' => Yii::t('app', 'Mode'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->start_time_int = $this->start_time;
        $this->allow_time_int = $this->allow_time;
        $this->start_time = \Yii::$app->formatter->asDatetime($this->start_time, 'd.MM.y H:mm');
        $this->allow_time = \Yii::$app->formatter->asDatetime($this->allow_time, 'd.MM.y H:mm');

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->start_time = strtotime($this->start_time);
        $this->allow_time = strtotime($this->allow_time);
        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentGroup()
    {
        return $this->hasOne(StudentGroup::className(), ['id' => 'student_group_id']);
    }

    public static function findAllByGroup() {
        return self::find()->where([
            'student_group_id' => Yii::$app->user->identity->student_group_id,
        ])->orderBy('start_time')->all();
    }


    /**
     * @param $userId
     * @return bool
     */
    public function isUserEnroll($userId = -1)
    {
        if ($userId == -1) {
            $userId = \Yii::$app->user->id;
        }
        return $this->getNumberInQueue($userId) != -1;
    }

    /**
     * @param $userId
     * @return int
     */
    public function getNumberInQueue($userId = -1)
    {
        if ($userId == -1) {
            $userId = \Yii::$app->user->id;
        }
        $eventRecords = $this->getEventRecords();
        $i = 0;
        foreach ($eventRecords as $record) {
            $i++;
            if ($record->user_id == $userId) {
                return $i;
            }
        }
        return -1;
    }

    public function getSizeQueue()
    {
        return count($this->getEventRecords());
    }

    /**
     * @return $this|EventRecord[]|array|null
     */
    public function getEventRecords()
    {
        if (!is_null($this->_eventRecords)) {
            return $this->_eventRecords;
        }
        $this->_eventRecords = EventRecord::getRecordsForEvent($this);
        return $this->_eventRecords;
    }

    public function getStatus($force = false)
    {
        if (!is_null($this->_status) && !$force) {
            return $this->_status;
        }
        $timeNow = time();
        if ($this->allow_time_int > $timeNow) {
            $this->_status = self::STATUS_WAIT;
        } else {
            $userId = \Yii::$app->user->id;
            if ($this->isUserEnroll($userId)) {
                $this->_status = self::STATUS_ENROLLED;
            } else {
                $this->_status = self::STATUS_CAN_ENROLL;
            }
        }
        return $this->_status;

    }

    public function getEnrollForm() {
        $enrollForm = new EnrollForm();
        $enrollForm->eventId = $this->id;
        return $enrollForm;
    }

    public static function getListMode()
    {
        return [
            self::MODE_DEFAULT => 'Обычная',
            self::MODE_SEX_WOMEN => 'Сначала девочки потом мальчики',
            self::MODE_SEX_MEN => 'Сначала мальчики потом девочки',
            self::MODE_SUBGROUP => 'По подгруппам',
        ];
    }
}
