<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%student_group}}".
 *
 * @property integer $id
 * @property integer $number
 * @property integer $schedule_student_group_id
 * @property integer $capitan_id
 */
class StudentGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%student_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'schedule_student_group_id', 'capitan_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'number' => Yii::t('app', 'STUDENT_GROUP_NUMBER'),
            'schedule_student_group_id' => Yii::t('app', 'Schedule Student Group ID'),
            'capitan_id' => Yii::t('app', 'Capitan ID'),
        ];
    }

    /**
     * @return User
     */
    public function getCaptain()
    {
        return $this->hasOne(User::className(), ['id' => 'capitan_id']);
    }

    /**
     * @param null $userStudentGroupId
     * @param null $firstElement
     * @return array|ActiveRecord[]
     */
    public static function getList($userStudentGroupId = null, $firstElement = null) {
        $studentGroups = self::find()->select(['id', 'number'])->where(
            (!is_null($userStudentGroupId)) ? [
                'id' => $userStudentGroupId,
            ] : []
        )->asArray()->all();
        $studentGroups = ArrayHelper::map($studentGroups, 'id', 'number');
        if (!is_null($firstElement)) {
            $studentGroups = ArrayHelper::merge([
                '0' => 'Все'
            ], $studentGroups);
        }
        return $studentGroups;
    }
}
