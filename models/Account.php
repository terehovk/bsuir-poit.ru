<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $client
 * @property integer $client_id
 * @property string $client_data
 * @property string $connection_string
 * @property integer $status
 */
class Account extends ActiveRecord
{
    const STATUS_CONNECTED = 0;
    const STATUS_WAIT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'client_id', 'status'], 'integer'],
            [['client_data'], 'string'],
            [['client', 'connection_string'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'client' => Yii::t('app', 'Client'),
            'client_id' => Yii::t('app', 'Client ID'),
            'client_data' => Yii::t('app', 'Client Data'),
            'connection_string' => Yii::t('app', 'Connection String'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @param string $connectionString
     * @return static|null
     */
    public static function findByConnectionString($connectionString)
    {
        return static::findOne(['connection_string' => $connectionString, 'status' => self::STATUS_WAIT]);
    }
}
