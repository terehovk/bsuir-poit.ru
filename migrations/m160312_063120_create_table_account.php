<?php

use yii\db\Migration;

class m160312_063120_create_table_account extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%account}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'client' => $this->string(),
            'client_id' => $this->integer(),
            'client_data' => $this->text(),
            'connection_string' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%account}}');
    }
}
