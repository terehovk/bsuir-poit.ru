<?php

use yii\db\Migration;

class m160329_100139_create_table_student_group extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%student_group}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(6),
            'schedule_student_group_id' => $this->integer(),
            'capitan_id' => $this->integer(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%student_group}}');
    }
}
