<?php

use yii\db\Migration;

class m160409_053242_create_table_event_record extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_record}}', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer(),
            'user_id' => $this->integer(),
            'comment' => $this->text(),
            'time_enroll' => $this->integer(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%event_record}}');
    }
}
