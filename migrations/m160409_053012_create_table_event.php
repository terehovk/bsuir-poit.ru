<?php

use yii\db\Migration;

class m160409_053012_create_table_event extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey(),
            'student_group_id' => $this->integer(),
            'student_subgroup' => $this->integer(),
            'start_time' => $this->integer(),
            'allow_time' => $this->integer(),
            'text' => $this->text(),
            'mode' => $this->integer(),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%event}}');
    }
}
