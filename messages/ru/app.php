<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Allow Time' => '',
    'Capitan ID' => '',
    'Client' => '',
    'Client Data' => '',
    'Client ID' => '',
    'Comment' => '',
    'Connection String' => '',
    'Create Event' => 'Добавить',
    'Create Student Group' => '',
    'Created At' => '',
    'Event ID' => '',
    'Events' => 'События',
    'HELLO {name}' => '',
    'ID' => '',
    'Mode' => '',
    'Photo' => '',
    'Role' => '',
    'Schedule Student Group ID' => '',
    'Start Time' => '',
    'Status' => '',
    'Student Groups' => 'Группы',
    'Text' => '',
    'Time Enroll' => '',
    'USER' => '',
    'Update {modelClass}: ' => '',
    'Updated At' => '',
    'User ID' => '',
    'Users' => 'Пользователи',
    'Auth Key' => '',
    'BUTTON_CREATE' => 'Добавить',
    'BUTTON_DELETE' => 'Удалить',
    'BUTTON_UPDATE' => 'Редактировать',
    'ERROR_PROFILE_BLOCKED' => 'Ваш аккаунт заблокирован.',
    'ERROR_PROFILE_NOT_CONFIRMED' => 'Ваш аккаунт не подтвержден.',
    'ERROR_USERNAME_EXISTS' => 'Это имя пользователя уже занято.',
    'ERROR_WRONG_USERNAME_OR_PASSWORD' => 'Неверное имя пользователя или пароль.',
    'Email Confirm Token' => '',
    'HELLO {username}' => 'Здравствуйте, {username}!',
    'Number' => '',
    'PLEASE_FILL_FOR_LOGIN' => 'Для входа на сайт введите данные своей учётной записи:',
    'PLEASE_FILL_FOR_SIGNUP' => 'Для регистрации заполните следующие поля:',
    'Password Hash' => '',
    'Password Reset Token' => '',
    'TITLE_CONTACT' => 'Обратная связь',
    'TITLE_LOGIN' => 'Вход',
    'TITLE_SIGNUP' => 'Регистрация',
    'USER_BUTTON_LOGIN' => 'Войти',
    'USER_BUTTON_SIGNUP' => 'Зарегистрироваться',
    'USER_CREATED' => 'Создан',
    'USER_PASSWORD' => 'Пароль',
    'USER_REMEMBER_ME' => 'Запомнить меня',
    'USER_ROLE' => 'Роль',
    'USER_STATUS' => 'Статус',
    'USER_STATUS_ACTIVE' => 'Активен',
    'USER_STATUS_BLOCKED' => 'Заблокирован',
    'USER_STATUS_WAIT' => 'Ожидает подтверждения',
    'USER_UPDATED' => 'Обновлён',
    'USER_USERNAME' => 'Имя пользователя',
    'USER_VERIFY_CODE' => 'Код',
    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить этот элемент?',
    'BUTTON_SAVE' => 'Сохранить',
    'BUTTON_SEND' => 'Отправить',
    'Content' => 'Текст',
    'Create' => 'Создать',
    'Create News' => 'Добавить новость',
    'Date' => 'Дата',
    'Delete' => 'Удалить',
    'ERROR_EMAIL_EXISTS' => 'Этот Email уже используется.',
    'ERROR_STUDENT_GROUP_NOT_EXISTS' => 'Этой группы не существует.',
    'ERROR_TOKEN_IS_SENT' => 'Токен уже отправлен.',
    'ERROR_USER_NOT_FOUND_BY_EMAIL' => 'Пользователь с таким адресом не найден.',
    'ERROR_WRONG_CURRENT_PASSWORD' => 'Неверный текущий пароль.',
    'Email' => 'E-mail',
    'FLASH_EMAIL_CONFIRM_ERROR' => 'Ошибка подтверждения Email.',
    'FLASH_EMAIL_CONFIRM_REQUEST' => 'Подтвердите ваш электронный адрес.',
    'FLASH_EMAIL_CONFIRM_SUCCESS' => 'Спасибо! Ваш Email успешно подтверждён.',
    'FLASH_PASSWORD_RESET_ERROR' => 'Извините. У нас возникли проблемы с отправкой.',
    'FLASH_PASSWORD_RESET_REQUEST' => 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.',
    'FLASH_PASSWORD_RESET_SUCCESS' => 'Спасибо! Пароль успешно изменён.',
    'FOLLOW_TO_CONFIRM_EMAIL' => 'Для подтверждения адреса пройдите по ссылке:',
    'FOLLOW_TO_RESET_PASSWORD' => 'Для смены пароля пройдите по ссылке:',
    'First Name' => 'Имя',
    'IGNORE_IF_DO_NOT_REGISTER' => 'Если Вы не регистрировались на нашем сайте, то просто удалите это письмо.',
    'LINK_PASSWORD_RESET' => 'Восстановить пароль',
    'Last Name' => 'Фамилия',
    'Login' => 'Войти',
    'News' => 'Новости',
    'PLEASE_FILL_FOR_RESET' => 'Введите новый пароль:',
    'PLEASE_FILL_FOR_RESET_REQUEST' => 'Введите свой Email и мы пришлём Вам инструкцию по восстановлению:',
    'Password' => 'Пароль',
    'Read more' => 'Читать далее...',
    'Remember me?' => 'Запомнить меня?',
    'SIGNUP_BUTTON' => 'Отправить',
    'STUDENT_GROUP_NUMBER' => 'Номер группы',
    'Signup' => 'Регистрация',
    'Student Group ID' => 'Группа',
    'Student Subgroup' => 'Подгруппа',
    'TITLE_PASSWORD_CHANGE' => 'Смена пароля',
    'TITLE_PASSWORD_RESET' => 'Восстановление пароля',
    'TITLE_PROFILE' => 'Профиль',
    'Title' => 'Заголовок',
    'USER_CURRENT_PASSWORD' => 'Текущий пароль',
    'USER_EMAIL' => 'E-mail',
    'USER_FIRST_NAME' => 'Имя',
    'USER_FULL_NAME' => 'Полное имя',
    'USER_ID' => 'ID',
    'USER_LAST_NAME' => 'Фамилия',
    'USER_NEW_PASSWORD' => 'Новый пароль',
    'USER_REPEAT_PASSWORD' => 'Повторите пароль',
    'USER_STUDENT_GROUP_ID' => 'Группа',
    'USER_STUDENT_SUBGROUP' => 'Подгруппа',
    'Update' => 'Редактировать',
];
